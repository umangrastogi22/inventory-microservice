package com.classpath.inventorymicroservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/inventory")
public class InventoryController {

    private static int counter = 1000;

    @GetMapping
    public int getInventory(){
        return counter;
    }

    @PostMapping
    public int updateQty(){
        return --counter;
    }
}